# Defined via `source`
function nf --wraps=neofetch --description 'alias nf neofetch'
  neofetch $argv; 
end

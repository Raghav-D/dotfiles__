(setq user-full-name "Raghav Dixit"
      user-mail-address "raghavd@disroot.org")

(setq org-support-shift-select t)

;; (setq shell-file-name "/opt/local/bin/fish") ; Set `fish' as default shell
(setq shell-file-name "/usr/bin/fish")   ; Set `nu' as default shell
(setq eshell-rc-script "~/.config/doom/eshell/profile"
      eshell-visual-commands '("bash" "fish" "htop" "ssh" "top" "zsh"))

(setq doom-theme 'doom-nord)
(set-face-attribute 'default nil
                    :font "JetBrainsMono NF 15"
                    :weight 'regular)

(set-face-attribute 'variable-pitch nil
                    :font "JetBrainsMono NF 14"
                    :weight 'medium)

(set-face-attribute 'fixed-pitch nil
                    :font "JetBrainsMono NF 15"
                    :weight 'regular)





(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t)
    (doom-themes-visual-bell-config))
(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))
(setq global-prettify-symbols-mode t)


(global-visual-line-mode t) ; enables word wrap
(setq kill-whole-line t)

(map! :leader
      :desc "Edit doom config"
      "- d c" #'(lambda () (interactive) (find-file "~/.config/doom/config.org"))
      :leader
      :desc "Edit doom packages"
      "- d p" #'(lambda () (interactive) (find-file "~/.config/doom/packages.el"))
      :leader
      :desc "Edit doom init"
      "- d i" #'(lambda () (interactive) (find-file "~/.config/doom/init.el"))
      :leader
      :desc "Edit eshell aliases"
      "- d a" #'(lambda () (interactive) (find-file "~/.config/doom/eshell/aliases"))
      :leader
      :desc "Edit fish config"
      "- f" #'(lambda () (interactive) (find-file "~/.config/fish/config.fish"))
      :leader
      :desc "Edit vifm config"
      "- v c" #'(lambda () (interactive) (find-file "~/.config/vifm/vifmrc"))
      :leader
      :desc "Rust Mode"
      "- r" #'rust-mode
      :leader
      :desc "Autocomplete mode"
      "- a" #'auto-complete-mode
      :leader
      :desc "Company Mode"
      "- c" #'company-mode
      :leader
      :desc "Toggle Neotree"
      "o n" #'neotree-toggle
      :leader
      :desc "Open Emacs Keybindings Cheat Sheet"
      "- e" #'(lambda () (interactive) (find-file "~/org-mode/emacs-vs-evil.org"))
      :leader
      :desc "Open mu4e"
      "o m" #'mu4e
      :leader
      :desc "Hasklig Mode"
      "- h" #'hasklig-mode
      :leader
      :desc "Fira Mono Mode"
      "- f" #'fira-code-mode)

(global-set-key (kbd "C-c k") #'evil-mode)

(map! :leader
      :desc "Counsel Find File"
      "- ." #'counsel-find-file)

(map! :leader
      "_" #'dmenu)

(after! org
  (defun efs/org-font-setup ()
	  ;; Replace list hyphen with dot
	  (font-lock-add-keywords 'org-mode
				  '(("^ *\\([-]\\) "
				     (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

	  ;; Set faces for heading levels
	  (dolist (face '((org-level-1 . 2.0)
			  (org-level-2 . 1.8)
			  (org-level-3 . 1.4)
			  (org-level-4 . 1.1)
			  (org-level-5 . 1.1)
			  (org-level-6 . 1.1)
			  (org-level-7 . 1.1)
			  (org-level-8 . 1.1)))
	    (set-face-attribute (car face) nil :font "Noto Sans" :weight 'regular :height (cdr face)))

	  ;; Ensure that anything that should be fixed-pitch in Org files appears that way
	  (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
	  (set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
	  (set-face-attribute 'org-table nil   :inherit '(shadow fixed-pitch))
	  (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
	  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
	  (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
	  (set-face-attribute 'org-checkbox nil :inherit 'varible-pitch))

	(use-package org

	  ;; :hook (org-mode . efs/org-mode-setup)
	  :config
	  (setq org-directory "~/org-mode/"
		org-agenda-files '("~/org-mode")
		org-default-notes-file (expand-file-name "notes.org" org-directory)
		org-ellipsis " ▼ "
		org-log-done 'time
		;; org-journal-dir "~/Org/journal/"
		org-hide-emphasis-markers t
		;; ex. of org-link-abbrev-alist in action
		;; [[arch-wiki:Name_of_Page][Description]]
		org-todo-keywords        ; This overwrites the default org-todo-keywords
		  '((sequence
		     "TODO(t)"
		     "HOMEWORK(h)"
		     "CLASS(C)"
		     "PROJ(p)"
		     "TEST(T)"
		     "EVENT(e)"
		     "EXAM(E)"
		     "|"
		     "DONE(d)"
		     "CANCELLED(c)"
		     "ANYTIME(a)")))
	  ;(setq org-ellipsis " ▾")
	  (efs/org-font-setup))
;; (defun dw/org-mode-visual-fill ()
;;   (setq visual-fill-column-width 110
        ;; visual-fill-column-center-text t)
  ;; (visual-fill-column-mode 1))

;; (use-package visual-fill-column
;;   :defer t
  ;; :hook (org-mode . dw/org-mode-visual-fill))


	(use-package org-bullets
	  :after org
	  :hook (org-mode . org-bullets-mode)
	  :custom
	  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))
	(use-package toc-org)
    (dolist (mode '(org-mode-hook))

		  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(defun nolinum ()
  (global-linum-mode 0))
(add-hook 'org-mode-hook 'nolinum))
  ;; (require 'org-superstar)
  ;; (add-hook 'org-mode-hook (lambda () (org-superstar-mode 1)))
  ;; ;; (use-package org-bullets
  ;;   ;; :ensure t
  ;;       ;; :init
  ;;       ;; (add-hook 'org-mode-hook (lambda ()
  ;;       ;; ;; (org-bullets-mode 1))))
  ;;       ;;


  ;; (defun my/org-mode/load-prettify-symbols ()
  ;;   (interactive)
  ;;   (setq prettify-symbols-alist
  ;;         (mapcan (lambda (x) (list x (cons (upcase (car x)) (cdr x))))
  ;;       '(("#+begin_src" . ?)
  ;;           ("#+end_src" . ?)
;            ("#+begin_example" . ?)
 ;           ("#+end_example" . ?)
  ;          ("#+DATE:" . ?⏱)
   ;         ("#+AUTHOR:" . ?✏)
    ;        ("[ ]" .  ?☐)
     ;       ("[X]" . ?☑ )
      ;      ("[-]" . ?❍ )
       ;     ("lambda" . ?λ)
        ;    ("#+header:" . ?)
         ;   ("#+name:" . ?﮸)
          ;  ("#+results:" . ?)
;            ("#+call:" . ?)
;            (":properties:" . ?)
;            (":logbook:" . ?))))
;    (prettify-symbols-mode 1));
;
;  (setq org-directory "~/org-mode/"
;        org-agenda-files '("~/org-mode")
;        org-default-notes-file (expand-file-name "notes.org" org-directory)
;        org-ellipsis " ▼ "
;        org-log-done 'time
;        org-journal-dir "~/Org/journal/"
;        org-journal-date-format "%B %d, %Y (%A) "
;        org-journal-file-format "%Y-%m-%d.org"
;        org-hide-emphasis-markeiirs t
;        ;; ex. of org-link-abbrev-alist in action
;        ;; [[arch-wiki:Name_of_Page][Description]]
;        org-link-abbrev-alist    ; This overwrites the default Doom org-link-abbrev-list
;          '(("google" . "http://www.google.com/search?q=")
;            ("ddg" . "https://duckduckgo.com/?q=")
;            ("wiki" . "https://en.wikipedia.org/wiki/"))
;        org-todo-keywords        ; This overwrites the default Doom org-todo-keywords
;          '((sequence
;             "TODO(t)"           ; A task that is ready to be tackled
;             "HOMEWORK(h)"
;             "CLASS(C)"
;             "PROJ(p)"
;             "TEST(T)"
;             "EVENT(e)"
;             "EXAM(E)"
;             "|"
;             "DONE(d)"
;             "CANCELLED(c)"
;             "ANYTIME(a)";

        ; (require 'org-bullets)
        ; (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
            ; ))))

(use-package org-tree-slide
  :custom
  (org-image-actual-width nil))

(define-key org-mode-map (kbd "<f8>") 'org-tree-slide-mode)
(define-key org-mode-map (kbd "S-<f8>") 'org-tree-slide-skip-done-toggle)

;; (setq centaur-tabs-set-bar 'over
;;       centaur-tabs-set-icons t
;;       centaur-tabs-gray-out-icons 'buffer
;;       centaur-tabs-height 24
;;       centaur-tabs-set-modified-marker t
;;       centaur-tabs-style "bar"
;;       centaur-tabs-modified-marker "•")
;; (map! :leader
;;       :desc "Toggle tabs globally" "t c" #'centaur-tabs-mode
;;       :desc "Toggle tabs local display" "t C" #'centaur-tabs-local-mode)
;; (evil-define-key 'normal centaur-tabs-mode-map (kbd "g <right>") 'centaur-tabs-forward        ; default Doom binding is 'g t'
;;                                                (kbd "g <left>")  'centaur-tabs-backward       ; default Doom binding is 'g T'
;;                                                (kbd "g <down>")  'centaur-tabs-forward-group
;;
;                                                (kbd "g <up>")    'centaur-tabs-backward-group)

(add-to-list 'initial-frame-alist '(fullscreen . maximized))
(setq confirm-kill-emacs nil)
(setq auto-save-default t
      make-backup-files t)

;; (load (expand-file-name "~/.quicklisp/slime-helper.el"))
;; (setq inferior-lisp-program "sbcl")

  (use-package dired-rainbow
    :defer 2
    :config
    (dired-rainbow-define-chmod directory "#6cb2eb" "d.*")
    (dired-rainbow-define html "#eb5286" ("css" "less" "sass" "scss" "htm" "html" "jhtm" "mht" "eml" "mustache" "xhtml"))
    (dired-rainbow-define xml "#f2d024" ("xml" "xsd" "xsl" "xslt" "wsdl" "bib" "json" "msg" "pgn" "rss" "yaml" "yml" "rdata"))
    (dired-rainbow-define document "#9561e2" ("docm" "doc" "docx" "odb" "odt" "pdb" "pdf" "ps" "rtf" "djvu" "epub" "odp" "ppt" "pptx"))
    (dired-rainbow-define markdown "#ffed4a" ("org" "etx" "info" "markdown" "md" "mkd" "nfo" "pod" "rst" "tex" "textfile" "txt"))
    (dired-rainbow-define database "#6574cd" ("xlsx" "xls" "csv" "accdb" "db" "mdb" "sqlite" "nc"))
    (dired-rainbow-define media "#de751f" ("mp3" "mp4" "mkv" "MP3" "MP4" "avi" "mpeg" "mpg" "flv" "ogg" "mov" "mid" "midi" "wav" "aiff" "flac"))
    (dired-rainbow-define image "#f66d9b" ("tiff" "tif" "cdr" "gif" "ico" "jpeg" "jpg" "png" "psd" "eps" "svg"))
    (dired-rainbow-define log "#c17d11" ("log"))
    (dired-rainbow-define shell "#f6993f" ("awk" "bash" "bat" "sed" "sh" "zsh" "vim"))
    (dired-rainbow-define interpreted "#38c172" ("py" "ipynb" "rb" "pl" "t" "msql" "mysql" "pgsql" "sql" "r" "clj" "cljs" "scala" "js"))
    (dired-rainbow-define compiled "#4dc0b5" ("asm" "cl" "lisp" "el" "c" "h" "c++" "h++" "hpp" "hxx" "m" "cc" "cs" "cp" "cpp" "go" "f" "for" "ftn" "f90" "f95" "f03" "f08" "s" "rs" "hi" "hs" "pyc" ".java"))
    (dired-rainbow-define executable "#8cc4ff" ("exe" "msi"))
    (dired-rainbow-define compressed "#51d88a" ("7z" "zip" "bz2" "tgz" "txz" "gz" "xz" "z" "Z" "jar" "war" "ear" "rar" "sar" "xpi" "apk" "xz" "tar"))
    (dired-rainbow-define packaged "#faad63" ("deb" "rpm" "apk" "jad" "jar" "cab" "pak" "pk3" "vdf" "vpk" "bsp"))
    (dired-rainbow-define encrypted "#ffed4a" ("gpg" "pgp" "asc" "bfe" "enc" "signature" "sig" "p12" "pem"))
    (dired-rainbow-define fonts "#6cb2eb" ("afm" "fon" "fnt" "pfb" "pfm" "ttf" "otf"))
    (dired-rainbow-define partition "#e3342f" ("dmg" "iso" "bin" "nrg" "qcow" "toast" "vcd" "vmdk" "bak"))
    (dired-rainbow-define vc "#0074d9" ("git" "gitignore" "gitattributes" "gitmodules"))
    (dired-rainbow-define-chmod executable-unix "#38c172" "-.*x.*"))

(map! :leader
      (:prefix ("d" . "dired")
       :desc "Open dired" "d" #'dired
       :desc "Dired jump to current" "j" #'dired-jump)
      (:after dired
       (:map dired-mode-map
        :desc "Peep-dired image previews" "d p" #'peep-dired
        :desc "Dired view file" "d v" #'dired-view-file)))
;; Make 'h' and 'l' go back and forward in dired. Much faster to navigate the directory structure!

(evil-define-key 'normal dired-mode-map
  (kbd "M-RET") 'dired-display-file
  (kbd "h") 'dired-up-directory
  (kbd "l") 'dired-open-file ; use dired-find-file instead of dired-open.
  (kbd "m") 'dired-mark
  (kbd "t") 'dired-toggle-marks
  (kbd "u") 'dired-unmark
  (kbd "C") 'dired-do-copy
  (kbd "D") 'dired-do-delete
  (kbd "J") 'dired-goto-file
  (kbd "M") 'dired-chmod
  (kbd "O") 'dired-chown
  (kbd "P") 'dired-do-print
  (kbd "R") 'dired-rename
  (kbd "T") 'dired-do-touch
  (kbd "Y") 'dired-copy-filenamecopy-filename-as-kill ; copies filename to kill ring.
  (kbd "+") 'dired-create-directory
  (kbd "-") 'dired-up-directory
  (kbd "% l") 'dired-downcase
  (kbd "% u") 'dired-upcase
  (kbd "; d") 'epa-dired-do-decrypt
  (kbd "; e") 'epa-dired-do-encrypt)
;; If peep-dired is enabled, you will get image previews as you go up/down with 'j' and 'k'
(evil-define-key 'normal peep-dired-mode-map
  (kbd "j") 'peep-dired-next-file
  (kbd "k") 'peep-dired-prev-file)
(add-hook 'peep-dired-hook 'evil-normalize-keymaps)
;; Get file icons in dired
(add-hook 'dired-mode-hook 'all-the-icons-dired-mode)
;; With dired-open plugin, you can launch external programs for certain extensions
;; For example, I set all .png files to open in 'sxiv' and all .mp4 files to open in 'mpv'
(setq dired-open-extensions '(("gif" . "sxiv")
                              ("jpg" . "sxiv")
                              ("png" . "sxiv")
                              ("mkv" . "mpv")
                              ("mp4" . "mpv")))
(use-package peep-dired
  :defer t ; don't access `dired-mode-map' until `peep-dired' is loaded
  :bind (:map dired-mode-map
         ("P" . peep-dired)))
  (use-package dired-single
    :defer t)

  (use-package dired-ranger
    :defer t)

  (use-package dired-collapse
    :defer t)

(use-package evil-collection)
(require 'evil-collection)

  (evil-collection-define-key 'normal 'dired-mode-map
    "H" 'dired-omit-mode
    "y" 'dired-ranger-copy
    "X" 'dired-ranger-move
    "p" 'dired-ranger-paste)

(map! :leader
      :desc "EWW web browser"
      "e w" #'eww
      :leader
      :desc "EWW reload page"
      "e R" #'eww-reload
      :leader
      :desc "Search web for text between BEG/END"
      "s w" #'eww-search-words)

(require 'elfeed-goodies)
(elfeed-goodies/setup)
(setq elfeed-goodies/entry-pane-size 0.5)
(defvar elfeed-feeds-alist
                    '(("https://odysee.com/$/rss/@DistroTube:2" Distrotube Odysee)
                     ("https://www.reddit.com/r/linux.rss" Linux Reddit)
                      ("https://www.reddit.com/r/commandline.rss" Linux Reddit)
                      ("http://opensource.com/feed" Linux Opensource)
                      ("https://distrowatch.com/news/dwd.xml" Linux Distrowatch)
                       ("https://reddit.com/r/emacs.rss")))
;; (setq browse-url-browser-function 'eww-browse-url)

(require 'auto-complete)
(global-auto-complete-mode t)
(require 'package)
(add-to-list 'package-archives
             '("melpa-stable" . "http://stable.melpa.org/packages/") t)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)

;(org-roam-d;; b-autosync-mode)
;; (add-to-list 'display-buffer-alist
;;              '("\\*org-roam\\*"
;;                (display-buffer-in-direction)
;;                (direction . right)
;;                (window-width . 0.33)
;;                (window-height . fit-window-to-buffer)))
;; (use-package org-roam
;;   :ensure t
;;   :init
;;   (setq org-roam-v2-ack t)
;;   :custom
;;   (org-roam-directory "~/org/roam")
;;   :bind (("C-c n l" . org-roam-buffer-toggle)
;;          ("C-c n f" . org-roam-node-field)
;;          ("C-c n i" . org-roam-node-/insert))
;;   :config
;;   (org-roam-
;   setup))

(use-package dashboard
  :init      ;; tweak dashboard config before loading it
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)
  (setq dashboard-banner-logo-title "\nKEYBINDINGS:\
\nFind file               (SPC .)     \
Open buffer list    (SPC b i)\
\nFind recent files       (SPC f r)   \
Open the eshell     (SPC e s)\
\nOpen dired file manager (SPC d d)   \
List of keybindings (SPC h b b)")
  (setq dashboard-startup-banner 'logo)) ;; use standard emacs logo as banner

;; display at `ivy-posframe-style'
;; (setq ivy-posframe-display-functions-alist '((t . ivy-posframe-display)))
;; (setq ivy-posframe-display-functions-alist '((t . ivy-posframe-display-at-frame-center)))
(setq ivy-posframe-display-functions-alist '((t . ivy-posframe-display-at-window-center)))
;; (setq ivy-posframe-display-functions-alist '((t . ivy-posframe-display-at-frame-bottom-left)))
;; (setq ivy-posframe-display-functions-alist '((t . ivy-posframe-display-at-window-bottom-left)))
;; (setq ivy-posframe-display-functions-alist '((t . ivy-posframe-display-at-frame-top-center)))
;; (ivy-posframe-mode 1)
;; (setq ivy-posframe-display-functions-alist
;;       '((swiper                     . ivy-posframe-display-at-point)
;;         (complete-symbol            . ivy-posframe-display-at-window-center)
;;         (counsel-M-x                . ivy-posframe-display-at-window-center)
;;         (counsel-esh-history        . ivy-posframe-display-at-window-center)
;;         (counsel-describe-function  . ivy-display-function-fallback)
;;         (counsel-describe-variable  . ivy-posframe-display-at-window-center)
;;         (counsel-find-file          . ivy-display-function-fallback)
;;         (counsel-recentf            . ivy-display-function-fallback)
;;         (counsel-register           . ivy-posframe-display-at-frame-bottom-window-center)
;;         (nil                        . ivy-posframe-display))
;;       ivy-posframe-height-alist
;;       '((swiper . 40)
;;         (t . 10)))
(ivy-posframe-mode 0) ; 1 enables posframe-mode, 0 disables it.

(use-package! elfeed-goodies)
(elfeed-goodies/setup)
(setq elfeed-goodies/entry-pane-size 0.5)
(add-hook 'elfeed-show-mode-hook 'visual-line-mode)
(evil-define-key 'normal elfeed-show-mode-map
  (kbd "J") 'elfeed-goodies/split-show-next
  (kbd "K") 'elfeed-goodies/split-show-prev)
(evil-define-key 'normal elfeed-search-mode-map
  (kbd "J") 'elfeed-goodies/split-show-next
  (kbd "K") 'elfeed-goodies/split-show-prev)

(defun prefer-horizontal-split ()
  (set-variable 'split-height-threshold nil t)
  (set-variable 'split-width-threshold 50 t))

(add-hook 'markdown-mode-hook 'prefer-horizontal-split)
(map! :leader
      :desc "Clone indirect buffer other window"
      "b c" #'clone-indirect-buffer-other-window)

;; (add-to-list 'load-path "/usr/local/share/emacs/site-lisp/mu/mu4e")
;; (require 'mu4e)

;; (setq mu4e-mu-home "~/.cache/mu")

;; (setq
;;  mu4e-headers-skip-duplicates  t
;;  mu4e-view-show-images t
;;  mu4e-view-show-addresses t
;;  mu4e-compose-format-flowed nil
;;  mu4e-date-format "%y/%m/%d"
;;  mu4e-headers-date-format "%Y/%m/%d"
;;  mu4e-change-filenames-when-moving t
;;  mu4e-attachments-dir "~/Downloads"

;;  mu4e-maildir       "~/Maildir"   ;; top-level Maildir
;;  ;; note that these folders below must start with /
;;  ;; the paths are relative to maildir root
;;  mu4e-refile-folder "/Archive"
;;  mu4e-sent-folder   "/Sent"
;;  mu4e-drafts-folder "/Drafts"
;;  mu4e-trash-folder  "/Trash")

;; ;; this setting allows to re-sync and re-index mail
;; ;; by pressing U
;; (setq mu4e-get-mail-command  "mbsync -a")

;; (use-package vterm)

(defun prefer-horizontal-split ()
  (set-variable 'split-height-threshold nil t)
  (set-variable 'split-width-threshold 40 t)) ; make this as low as needed
(add-hook 'markdown-mode-hook 'prefer-horizontal-split)
(map! :leader
      :desc "Clone indirect buffer other window" "b c" #'clone-indirect-buffer-other-window)

(setq inferior-lisp-program "sbcl")
